import { Controller, Get, Param } from '@nestjs/common';
import { IBoleto } from '../interface';
import { BoletoService } from '../service';

@Controller('boleto')
export class BoletoController {
  constructor(private readonly boletoService: BoletoService) {}

  @Get(':barCode')
  decode(@Param('barCode') barCode: string): IBoleto {
    const decoded = this.boletoService.perform(barCode);

    return decoded;
  }
}
