import { Injectable } from '@nestjs/common';
import { BarCodeType, BoletoType } from '../enum';
import { BoletoError } from '../error';
import { IBoletoIdentify } from '../interface';

@Injectable()
export class IdentifyBoletoService {
  identifyBoletoType(barCode: string): BoletoType {
    if (barCode[0] === '8') {
      if (barCode.length === 44 || barCode.length === 48)
        return BoletoType.convenio;

      throw BoletoError.SIZE_OF_BAR_CODE_IS_INVALID;
    }

    return BoletoType.titulo;
  }

  identifyBarCode(barCode: string): BarCodeType {
    if (barCode.length === 44) {
      return BarCodeType.readable;
    }

    if (barCode.length === 48 || barCode.length === 47) {
      return BarCodeType.digitable;
    }

    throw BoletoError.SIZE_OF_BAR_CODE_IS_INVALID;
  }

  perform(barCode: string): IBoletoIdentify {
    return {
      barCodeType: this.identifyBarCode(barCode),
      boletoType: this.identifyBoletoType(barCode),
    };
  }
}
