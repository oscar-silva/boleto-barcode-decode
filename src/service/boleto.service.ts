import { Injectable, Logger } from '@nestjs/common';
import { BoletoError } from '../error';
import { IBoleto } from '../interface';
import { DecodeBoletoService, ValidateDigitBoletoService } from '../service';

@Injectable()
export class BoletoService {
  constructor(
    private readonly decodeBoletoService: DecodeBoletoService,
    private readonly validateDigitBoletoService: ValidateDigitBoletoService,
  ) {}

  validateBarcode(barCode: string) {
    if (barCode.length in [44, 47, 48])
      throw BoletoError.SIZE_OF_BAR_CODE_IS_INVALID;

    if (!/^\d+$/.test(barCode)) throw BoletoError.ONLY_NUMBERS_ALLOWED;
  }

  perform(barCode: string): IBoleto {
    this.validateBarcode(barCode);
    this.validateDigitBoletoService.perform(barCode);

    return this.decodeBoletoService.perform(barCode);
  }
}
