import { Injectable } from '@nestjs/common';
import { BarCodeType, BoletoType } from '../enum';
import { BoletoError } from '../error';
import { IBoletoField, IBoletoIdentify } from '../interface';
import { IdentifyBoletoService } from '../service';

@Injectable()
export class ValidateDigitBoletoService {
  constructor(private readonly identifyBoletoService: IdentifyBoletoService) {}

  calcDAC10(field: string): number {
    const elements = field
      .split('')
      .reverse()
      .map((n) => Number(n));

    let multiplierBase = 2;
    let total = 0;

    for (const n of elements) {
      let multiplication = n * multiplierBase;

      if (multiplication > 9) {
        multiplication =
          Number(multiplication.toString()[0]) +
          Number(multiplication.toString()[1]);
      }

      multiplierBase = multiplierBase === 2 ? 1 : 2;

      total = total + multiplication;
    }

    const dv = 10 - (total % 10);

    if (dv === 10) return 0;

    return dv;
  }

  calcDAC11(barCode: string, boletoIdentify: IBoletoIdentify): any {
    const elements = barCode
      .split('')
      .reverse()
      .map((n) => Number(n));

    let multiplierBase = 2;
    let total = 0;

    for (const n of elements) {
      const multiplication = n * multiplierBase;

      if (multiplierBase >= 9) {
        multiplierBase = 2;
      } else {
        multiplierBase++;
      }

      total = total + multiplication;
    }

    const dv = 11 - (total % 11);

    if (boletoIdentify.boletoType === BoletoType.convenio) {
      if (total % 11 in [0, 1]) return 0;
      if (total % 11 === 10) return 1;
    }

    if (boletoIdentify.boletoType === BoletoType.titulo)
      if (dv in [0, 10, 11]) return 1;

    return dv;
  }

  separateTituloDigitableFields(barCode: string) {
    const digitableFields: IBoletoField[] = [
      {
        start: 0,
        end: 9,
        size: 10,
        value: '',
      },
      {
        start: 10,
        end: 20,
        size: 11,
        value: '',
      },
      {
        start: 21,
        end: 31,
        size: 11,
        value: '',
      },
      {
        start: 32,
        end: 32,
        size: 1,
        value: '',
      },
      {
        start: 33,
        end: 46,
        size: 14,
        value: '',
      },
    ];

    return digitableFields.map((field) => {
      const value = barCode.substring(field.start, field.end + 1);
      field.value = value;
      return field;
    });
  }

  validateTitulo(barCode: string, boletoIdentify: IBoletoIdentify) {
    if (boletoIdentify.barCodeType === BarCodeType.digitable) {
      const fields = this.separateTituloDigitableFields(barCode)
        .filter((f, i) => i < 3)
        .map((f) => f.value.substring(0, f.value.length));

      fields.forEach((dv) => {
        const receivedDV = Number(dv[dv.length - 1]);
        const calculatedDV = this.calcDAC10(dv.substring(0, dv.length - 1));

        if (receivedDV !== calculatedDV) {
          throw BoletoError.INVALID_CHECK_DIGIT;
        }
      });
    }

    if (boletoIdentify.barCodeType === BarCodeType.readable) {
      const receivedDV = Number(barCode[4]);
      const barCodeWithoutDV =
        barCode.substring(0, 4) + barCode.substring(5, 44);
      const calculatedDV = this.calcDAC11(barCodeWithoutDV, boletoIdentify);

      if (receivedDV !== calculatedDV) {
        throw BoletoError.INVALID_CHECK_DIGIT;
      }
    }
  }

  validateConvenio(barCode: string, boletoIdentify: IBoletoIdentify) {
    if (boletoIdentify.barCodeType === BarCodeType.digitable) {
      const reicevedDVG = Number(barCode[3]);
      const currencyCode = Number(barCode[2]);
      const barCodeToCalculate = barCode.substring(0, 3) + barCode.substring(5);
      const calculatedDVG =
        currencyCode in [6, 7]
          ? this.calcDAC10(barCodeToCalculate)
          : this.calcDAC11(barCodeToCalculate, boletoIdentify);

      if (calculatedDVG !== reicevedDVG) {
        throw BoletoError.INVALID_CHECK_DIGIT;
      }
    }

    if (boletoIdentify.barCodeType === BarCodeType.readable) {
      const reicevedDVG = Number(barCode[3]);
      const barCodeToCalculate = barCode.substring(0, 3) + barCode.substring(4);
      const calculatedDVG = this.calcDAC11(barCodeToCalculate, boletoIdentify);

      if (calculatedDVG !== reicevedDVG) {
        throw BoletoError.INVALID_CHECK_DIGIT;
      }
    }
  }

  perform(barCode: string): boolean {
    const boletoIdentify = this.identifyBoletoService.perform(barCode);

    if (boletoIdentify.boletoType === BoletoType.titulo)
      this.validateTitulo(barCode, boletoIdentify);

    if (boletoIdentify.boletoType === BoletoType.convenio)
      this.validateConvenio(barCode, boletoIdentify);

    return true;
  }
}
