import { Injectable } from '@nestjs/common';
import { BarCodeType, BoletoType } from '../enum';
import { IBoleto, IBoletoIdentify } from '../interface';
import { IdentifyBoletoService } from '../service';

@Injectable()
export class DecodeBoletoService {
  constructor(private readonly identifyBoletoService: IdentifyBoletoService) {}

  boletoIdentify: IBoletoIdentify;

  converAmount(baseAmount: string) {
    return (
      baseAmount.substring(0, baseAmount.length - 2) +
      '.' +
      baseAmount.substring(baseAmount.length - 2)
    );
  }

  convertDate(baseDate: string): string {
    const initialDate = new Date('10/07/1997');
    initialDate.setDate(initialDate.getDate() + parseInt(baseDate));

    return initialDate.toLocaleDateString('pt-BR');
  }

  decodeTitulo(barCode: string): IBoleto {
    const baseAmount =
      this.boletoIdentify.barCodeType === BarCodeType.readable
        ? barCode.substring(9, 19)
        : barCode.substring(barCode.length - 10, barCode.length);

    const amount = this.converAmount(baseAmount);

    const expirationDateBase =
      this.boletoIdentify.barCodeType === BarCodeType.readable
        ? barCode.substring(5, 9)
        : barCode.substring(33, 37);

    const expirationDate: string | null =
      this.convertDate(expirationDateBase) === '07/10/1997'
        ? null
        : this.convertDate(expirationDateBase);

    return {
      barCode,
      amount: Number(amount),
      expirationDate,
    };
  }

  decodeConvenio(barCode: string): IBoleto {
    const baseAmount = barCode.substring(4, 15);

    const amount = this.converAmount(baseAmount);

    const expirationDateBase = barCode.substring(23, 31);
    const expirationDate = `${expirationDateBase.substring(
      6,
      8,
    )}/${expirationDateBase.substring(4, 6)}/${expirationDateBase.substring(
      0,
      4,
    )}`;

    const isInvalidDate = (date: Date) => Number.isNaN(date.getTime());

    return {
      barCode,
      amount: Number(amount),
      expirationDate: isInvalidDate(new Date(expirationDate))
        ? null
        : expirationDate,
    };
  }

  perform(barCode: string): IBoleto {
    this.boletoIdentify = this.identifyBoletoService.perform(barCode);

    if (this.boletoIdentify.boletoType === BoletoType.titulo)
      return this.decodeTitulo(barCode);

    return this.decodeConvenio(barCode);
  }
}
