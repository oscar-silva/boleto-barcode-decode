import { Module } from '@nestjs/common';
import { BoletoController } from './controller';
import {
  BoletoService,
  DecodeBoletoService,
  IdentifyBoletoService,
  ValidateDigitBoletoService,
} from './service';

@Module({
  imports: [],
  controllers: [BoletoController],
  providers: [
    DecodeBoletoService,
    IdentifyBoletoService,
    ValidateDigitBoletoService,
    BoletoService,
  ],
})
export class AppModule {}
