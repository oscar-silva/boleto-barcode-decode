import { BadRequestException } from '@nestjs/common';

export class BoletoError {
  static ONLY_NUMBERS_ALLOWED: BadRequestException = new BadRequestException(
    'ONLY_NUMBERS_ALLOWED',
  );
  static SIZE_OF_BAR_CODE_IS_INVALID: BadRequestException =
    new BadRequestException('SIZE_OF_BAR_CODE_IS_INVALID');
  static INVALID_CHECK_DIGIT: BadRequestException = new BadRequestException(
    'INVALID_CHECK_DIGIT',
  );
}
