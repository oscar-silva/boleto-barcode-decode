export enum FieldContent {
  bankCode = 'bankCode',
  currencyType = 'currencyType',
  validatorDigit = 'validatorDigit',
  expirationDateFactor = 'expirationDate',
  amountValue = 'amountValue',
  freeField = 'freeField',
}

export interface IBoletoField {
  size: number;
  start: number;
  end: number;
  value: string;
  type?: FieldContent;
}
