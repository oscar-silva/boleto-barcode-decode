import { BarCodeType, BoletoType } from '../enum';

export interface IBoleto {
  barCode: string;
  amount: number;
  expirationDate: string;
}

export interface IBoletoIdentify {
  barCodeType: BarCodeType;
  boletoType: BoletoType;
}
