import { INestApplication } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { AppModule } from '../app.module';
import supertest from 'supertest';

export abstract class BaseTest {
  static app: INestApplication;
  static httpServer: any;

  static async before(): Promise<void> {
    const moduleRef = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    BaseTest.app = await moduleRef.createNestApplication().init();
    BaseTest.httpServer = this.app.getHttpServer();
  }

  server(): supertest.SuperTest<supertest.Test> {
    return supertest(BaseTest.httpServer);
  }
}
