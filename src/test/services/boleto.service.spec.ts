import { Test, TestingModule } from '@nestjs/testing';
import { BoletoError } from '../../error';
import { AppModule } from '../../app.module';
import { BoletoController } from '../../controller';
import { BoletoService } from '../../service';
import { BadRequestException, HttpException } from '@nestjs/common';

describe('BoletoService', () => {
  let app: TestingModule;
  let boletoService: BoletoService;

  beforeEach(async () => {
    app = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    boletoService = app.get<BoletoService>(BoletoService);
  });

  test('Should return the decoded boleto data', () => {
    expect(
      boletoService.perform('21290001192110001210904475617405975870000002000'),
    ).toStrictEqual({
      barCode: '21290001192110001210904475617405975870000002000',
      amount: 20,
      expirationDate: '16/07/2018',
    });
  });

  test('Should return ONLY_NUMBER_ALLOWED if have words params', () => {
    expect(() =>
      boletoService.perform('21290001192110001210904475617405975870000002000a'),
    ).toThrowError(new HttpException(BoletoError.ONLY_NUMBERS_ALLOWED, 400));
  });

  test('Should return SIZE_OF_BAR_CODE_IS_INVALID if size of barcode not in 44, 47 or 48', () => {
    expect(() => boletoService.perform('1123')).toThrowError(
      new HttpException(BoletoError.SIZE_OF_BAR_CODE_IS_INVALID, 400),
    );
  });
});
