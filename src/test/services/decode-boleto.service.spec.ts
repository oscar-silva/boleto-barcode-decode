import { AppModule } from '../../app.module';
import { Test, TestingModule } from '@nestjs/testing';
import { DecodeBoletoService } from '../../service';
import { INestApplication } from '@nestjs/common';

describe('DecodeBoletoService', () => {
  let app: TestingModule;
  let decodeBoletoService: DecodeBoletoService;

  beforeEach(async () => {
    app = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    decodeBoletoService = app.get(DecodeBoletoService);
    console.log(
      '🚀 ~ file: decode-boleto.service.spec.ts ~ line 30 ~ describe ~ decodeBoletoService',
      decodeBoletoService,
    );
  });

  describe('Decode digitable boleto convenio and return correct values', () => {
    expect(
      decodeBoletoService.perform(
        '82200000215048200974123220154098290108605940',
      ),
    ).toBe({
      barCode: '82200000215048200974123220154098290108605940',
      amount: 21504.82,
      expirationDate: null,
    });
  });
});
