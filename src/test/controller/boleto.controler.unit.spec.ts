import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '../../app.module';
import { BoletoController } from '../../controller';

describe('[UNITARY] Boleto controller', () => {
  let app: TestingModule;
  let boletoController: BoletoController;

  beforeEach(async () => {
    app = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    boletoController = app.get<BoletoController>(BoletoController);
  });

  test('Should return decoded boleto data', async () => {
    expect(
      boletoController.decode(
        '21290001192110001210904475617405975870000002000',
      ),
    ).toStrictEqual({
      barCode: '21290001192110001210904475617405975870000002000',
      amount: 20,
      expirationDate: '16/07/2018',
    });
  });
});
