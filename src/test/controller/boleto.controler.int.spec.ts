import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '../../app.module';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';

describe('[Integration] Boleto controller', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  test('GET - /boleto/:id - Should return decoded boleto', async () => {
    return request(app.getHttpServer())
      .get('/boleto/21290001192110001210904475617405975870000002000')
      .expect(200)
      .expect({
        barCode: '21290001192110001210904475617405975870000002000',
        amount: 20,
        expirationDate: '16/07/2018',
      });
  });

  test('GET - /boleto/:id - Should return ONLY_NUMBER_ALLOWED if have words in path param :id', async () => {
    return request(app.getHttpServer())
      .get('/boleto/21290001192110001210904475617405975870000002000a')
      .expect(400)
      .expect({
        statusCode: 400,
        error: 'Bad Request',
        message: 'ONLY_NUMBERS_ALLOWED',
      });
  });

  test('GET - /boleto/:id - Should return SIZE_OF_BAR_CODE_IS_INVALID if size not in 44, 47, 48', async () => {
    return request(app.getHttpServer())
      .get('/boleto/21290001192110001210904475617405975870000002000123123123')
      .expect(400)
      .expect({
        statusCode: 400,
        error: 'Bad Request',
        message: 'SIZE_OF_BAR_CODE_IS_INVALID',
      });
  });
});
