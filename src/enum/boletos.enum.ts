export enum BoletoType {
  titulo = 'titulo',
  convenio = 'convenio',
}

export enum BarCodeType {
  digitable = 'digitable',
  readable = 'readable',
}
