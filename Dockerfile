FROM node:alpine as common-build-stage
RUN mkdir -p /app
COPY . ./app
WORKDIR /app
RUN npm install
EXPOSE 8080

FROM common-build-stage as development-build-stage
CMD ["npm", "start"]